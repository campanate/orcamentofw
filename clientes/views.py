from django.shortcuts import render
from .models import Empresa, Telefone
from django.core.paginator import Paginator
from .forms import TelefoneForm, EmpresaForm
from django.shortcuts import get_object_or_404


# Create your views here.

def index(request, page=None):
    page = page or 1
    lista = Empresa.objects.all()
    paginator = Paginator(lista, 10)
    pagedList = paginator.page(page)
    return render(request, 'clientes/index.html', {'empresas': pagedList, 'title': 'Lista de Empresas'})


def create(request):
    form = EmpresaForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            empresa = form.save()
            telefones = request.POST.getlist('telefone')
            for telefone in telefones:
                tel = Telefone(numero=telefone, empresa=empresa)
                tel.save()
            return index(request)
    return render(request, 'clientes/create.html', {'form': form, 'title': 'criar empresa'})


def details(request, empresa_id):
    empresa = get_object_or_404(Empresa, pk=empresa_id)
    telefones = Telefone.objects.filter(empresa=empresa)
    return render(request, 'clientes/details.html', {'empresa': empresa, 'telefones': telefones, 'title' : 'Detalhes de empresa'})


def delete(request, empresa_id):
    empresa = get_object_or_404(Empresa, pk=empresa_id)
    empresa.delete()
    return index(request)