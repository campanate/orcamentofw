from __future__ import unicode_literals

from django.db import models


class Empresa(models.Model):
    nome = models.CharField(max_length=100)
    endereco = models.CharField(max_length=150)
    cep = models.CharField(max_length=20)
    municipio = models.CharField(max_length=100)
    estado = models.CharField(max_length=100)
    CNPJ = models.CharField(max_length=100, null=True)
    email = models.EmailField(max_length=100)
    datacadastro = models.DateField(auto_now=True, null=True)

    def __str__(self):
        return str(self.nome)


class Telefone(models.Model):
    numero = models.CharField(max_length=20)
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE)
    datacadastro = models.DateField(auto_now=True, null=True)
