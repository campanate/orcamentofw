# coding=utf-8
from django import forms

from .models import Empresa, Telefone


class TelefoneForm(forms.Form):
    numero = forms.CharField(max_length=100, widget=forms.TextInput({'placeholder': 'Número', 'class': 'form-control'}))


class EmpresaForm(forms.ModelForm):
    class Meta:
        model = Empresa
        fields = "__all__"
        widgets = {
            'nome': forms.TextInput({'placeholder': 'Nome', 'class': 'form-control'}),
            'endereco': forms.TextInput({'placeholder': 'Endereço', 'class': 'form-control'}),
            'cep': forms.TextInput({'placeholder': 'CEP', 'class': 'form-control'}),
            'municipio': forms.TextInput({'placeholder': 'Munícipio', 'class': 'form-control'}),
            'estado': forms.TextInput({'placeholder': 'UF', 'class': 'form-control'}),
            'email': forms.EmailInput({'placeholder': 'Email', 'class': 'form-control'}),
            'CNPJ': forms.TextInput({'placeholder': 'CNPJ', 'class': 'form-control'})

        }
