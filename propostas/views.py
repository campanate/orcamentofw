#coding=utf8
from django.shortcuts import render
from .models import Proposta
from .forms import PropostaForm

from django.core.paginator import Paginator
from django.shortcuts import get_object_or_404


def index(request, page=None):
    page = page or 1
    propostas = Proposta.objects.all()
    paginator = Paginator(propostas, 15)
    return render(request, 'propostas/index.html', {'title': 'Lista de Propostas',
                                                    'propostas': paginator.page(page)})


def create(request):
    form = PropostaForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return index(request)
    return render(request, 'propostas/create.html', {'title': 'Nova proposta',
                                                     'form': form})


def edit(request, proposta_id=None):
    proposta = get_object_or_404(Proposta, pk=proposta_id)
    form = PropostaForm(request.POST or None, instance=proposta)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return index(request)
    return render(request, 'propostas/edit.html', {'title': 'Editar proposta',
                                                   'form': form})