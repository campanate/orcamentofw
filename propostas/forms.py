#coding=utf8
from django import forms
from .models import Proposta


class PropostaForm(forms.ModelForm):
    class Meta:
        model = Proposta
        fields = '__all__'
        widgets = {
            'descricao': forms.TextInput({'class': 'form-control', 'placeholder': 'Descrição'})
        }

