#coding=utf8
from __future__ import unicode_literals

from django.db import models


class Proposta(models.Model):
    descricao = models.CharField(max_length=100)
    pub_data = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.descricao)

    class Meta:
        db_table = 'propostas'
