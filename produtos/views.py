from django.shortcuts import render
from django.core.paginator import Paginator
from .models import Produto
from .forms import ProdutoForm
from django.shortcuts import get_object_or_404


def index(request, page=None):
    page = page or 1
    lista = Produto.objects.all()
    paginator = Paginator(lista, 10)
    return render(request, 'produtos/index.html', {'produtos': paginator.page(page), 'title': 'Lista de produtos'})


def create(request):
    form = ProdutoForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return index(request)
    return render(request, 'produtos/create.html', {'form': form, 'title': 'Novo produto'})


def details(request, produto_id):
    produto = get_object_or_404(Produto, pk=produto_id)
    return render(request, 'produtos/details.html', {'produto': produto, 'title': 'Detalhes de Produto'})


def edit(request, produto_id):
    produto = get_object_or_404(Produto, pk=produto_id)
    form = ProdutoForm(request.POST or None, instance=produto)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return index(request)
    return render(request, 'produtos/edit.html', {'form': form, 'title': 'Editar produto'})


def delete(request, produto_id):
    produto = get_object_or_404(Produto, pk=produto_id)
    produto.delete()
    return index(request)