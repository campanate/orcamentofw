from __future__ import unicode_literals

from django.db import models


class Produto(models.Model):
    equipamento = models.CharField(max_length=100)
    valor = models.DecimalField(decimal_places=2, max_digits=10)



    class Meta:
        db_table = 'Produtos'

