from django import forms
from .models import Produto


class ProdutoForm(forms.ModelForm):

    class Meta:
        model = Produto
        fields = '__all__'
        widgets = {
            'equipamento': forms.TextInput({'placeholder': 'Equipamento', 'class': 'form-control'}),
            'valor': forms.NumberInput({'placeholder': 'Valor', 'class': 'form-control'}),

        }