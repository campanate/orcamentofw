from __future__ import unicode_literals


from clientes.models import Empresa
from produtos.models import Produto
from servicos.models import Servico
from propostas.models import Proposta
from django.db import models


class Orcamento(models.Model):
    tipo = models.CharField(max_length=100)
    vendedor = models.ForeignKey(Empresa, null=True, related_name='vendedor')
    comprador = models.ForeignKey(Empresa, null=True, related_name='comprador')
    produtos = models.ManyToManyField(Produto, through='OrcamentoProduto', related_name='possui_produtos', blank=True)
    servicos = models.ManyToManyField(Servico, through='OrcamentoServico', related_name='possui_servicos', blank=True)
    proposta = models.ForeignKey(Proposta, on_delete=models.CASCADE)
    observacao = models.CharField(max_length=500, blank=True, null=True)
    pub_data = models.DateField(auto_now=True, null=True)

    @property
    def total(self):
        return sum(c.total for c in self.produtos) + sum(c.total for c in self.servicos)

    class Meta:
        db_table = 'Orcamentos'


class OrcamentoProduto(models.Model):
    orcamento = models.ForeignKey('orcamentos.Orcamento')
    produto = models.ForeignKey(Produto)
    quantidade = models.IntegerField()
    pub_data = models.DateTimeField(auto_now_add=True)

    @property
    def total(self):
        return self.produto.valor*self.quantidade
    
    class Meta:
        db_table = 'OrcamentosProdutos'


class OrcamentoServico(models.Model):
    orcamento = models.ForeignKey('orcamentos.Orcamento')
    servico = models.ForeignKey(Servico)
    quantidade = models.IntegerField()
    pub_data = models.DateTimeField(auto_now_add=True)

    @property
    def total(self):
        return self.servico.valor*self.quantidade

    class Meta:
        db_table = 'OrcamentosServicos'


