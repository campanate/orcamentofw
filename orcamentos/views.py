#coding=utf-8
from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator
from .models import Orcamento, OrcamentoProduto, OrcamentoServico
from produtos.models import Produto
from servicos.models import Servico
from clientes.models import Empresa, Telefone
from propostas.models import Proposta
from .forms import OrcamentoForm


def index(request, page=None):
    page = page or 1
    cliente = request.GET.get('Search') or ''
    lista = Orcamento.objects.filter(vendedor__nome__contains=cliente)
    paginator = Paginator(lista, 10)
    return render(request, 'orcamentos/index.html', {'orcamentos': paginator.page(page), 'title': 'Lista de Orçamentos'})


def create(request):
    form = OrcamentoForm(request.POST or None)
    if request.method == 'POST':
        id_vendedor = request.POST.get('vendedor')
        id_comprador = request.POST.get('comprador')
        id_proposta = request.POST.get('proposta')
        proposta = get_object_or_404(Proposta, pk=id_proposta)
        vendedor = get_object_or_404(Empresa, pk=id_vendedor)
        comprador = get_object_or_404(Empresa, pk=id_comprador)
        orcamento = Orcamento(vendedor=vendedor, comprador=comprador,proposta=proposta)
        orcamento.save()
        prods = request.POST.getlist('produtos')
        qntprod = list(map(int, request.POST.getlist('quantidade-produtos')))

        qntserv = list(map(int, request.POST.getlist('quantidade-servicos')))

        servs = request.POST.getlist('servicos')
        for i, prod in enumerate(prods):
            try:
                produto = Produto.objects.get(id=prod)
                orcprod = OrcamentoProduto(produto=produto,quantidade=qntprod[i],orcamento=orcamento)
                orcprod.save()
            except Produto.DoesNotExist:
                produto = None
        for i, serv in enumerate(servs):
            try:
                servico = Servico.objects.get(id=prod)
                orcserv = OrcamentoServico(servico=servico, quantidade=qntserv[i], orcamento=orcamento)
                orcserv.save()
            except Servico.DoesNotExist:
                servico = None
        return index(request)

    produtos = Produto.objects.all()
    servicos = Servico.objects.all()
    return render(request, 'orcamentos/create.html', {'form': form, 'title': 'Novo orçamento', 'produtos': produtos, 'servicos': servicos})


def details(request, orcamento_id):
    orcamento = get_object_or_404(Orcamento, pk=orcamento_id)
    telefones_vendedor = Telefone.objects.filter(empresa=orcamento.vendedor)
    telefones_comprador = Telefone.objects.filter(empresa=orcamento.comprador)
    produtos = OrcamentoProduto.objects.filter(orcamento=orcamento)
    servicos = OrcamentoServico.objects.filter(orcamento=orcamento)
    total = sum(c.total for c in servicos) + sum(c.total for c in produtos)
    return render(request, 'orcamentos/details.html', {'orcamento': orcamento, 'servicos': servicos,
                                                       'produtos': produtos,
                                                       'telefones_vendedor': telefones_vendedor,
                                                       'telefones_comprador': telefones_comprador,
                                                       'title': 'Detalhes de orçamento',
                                                       'total': total})


def export(request, orcamento_id):
    orcamento = get_object_or_404(Orcamento, pk=orcamento_id)
    telefones_vendedor = Telefone.objects.filter(empresa=orcamento.vendedor)
    telefones_vendedor = ' '.join(c.numero for c in telefones_vendedor)
    telefones_comprador = Telefone.objects.filter(empresa=orcamento.comprador)
    telefones_comprador = ' '.join(c.numero for c in telefones_comprador)
    produtos = OrcamentoProduto.objects.filter(orcamento=orcamento)
    servicos = OrcamentoServico.objects.filter(orcamento=orcamento)
    totalprod = sum(c.total for c in produtos)
    totalserv = sum(c.total for c in servicos)
    total = sum(c.total for c in servicos) + sum(c.total for c in produtos)
    return render(request, 'orcamentos/pdf.html', {'orcamento': orcamento, 'servicos': servicos,
                                                       'produtos': produtos,
                                                       'telefones_vendedor': telefones_vendedor,
                                                        'telefones_comprador': telefones_comprador,
                                                       'title': 'Detalhes de orçamento',
                                                       'total': total,
                                                        'totalprod': totalprod,
                                                        'totalserv': totalserv})



def delete(request, orcamento_id):
    orcamento = get_object_or_404(Orcamento, pk=orcamento_id)
    orcamento.delete()
    return index(request)


def edit(request, orcamento_id):
    orcamento = get_object_or_404(Orcamento, pk=orcamento_id)
    form = OrcamentoForm(request.POST or None, instance=orcamento)
    produtos_orcamento = OrcamentoProduto.objects.filter(orcamento=orcamento)
    servicos_orcamento = OrcamentoServico.objects.filter(orcamento=orcamento)
    if request.method == "POST":
        if form.is_valid():
            orcamento.save()
            produtos_orcamento.delete()
            servicos_orcamento.delete()
            prods = request.POST.getlist('produtos')
            qntprod = list(map(int, request.POST.getlist('quantidade-produtos')))
            qntserv = list(map(int, request.POST.getlist('quantidade-servicos')))
            servs = request.POST.getlist('servicos')
            for i, prod in enumerate(prods):
                try:
                    produto = Produto.objects.get(id=prod)
                    orcprod = OrcamentoProduto(produto=produto,quantidade=qntprod[i],orcamento=orcamento)
                    orcprod.save()
                except Produto.DoesNotExist:
                    produto = None
            for i, serv in enumerate(servs):
                try:
                    servico = Servico.objects.get(id=prod)
                    orcserv = OrcamentoServico(servico=servico, quantidade=qntserv[i], orcamento=orcamento)
                    orcserv.save()
                except Servico.DoesNotExist:
                    servico = None
            return index(request)
    c = {
        'form': form,
        'produtos_orcamento': produtos_orcamento,
        'servicos_orcamento': servicos_orcamento,
        'produtos': Produto.objects.all(),
        'servicos': Servico.objects.all(),
        'title': 'Editar orçamento'

    }
    return render(request, 'orcamentos/edit.html', c)


