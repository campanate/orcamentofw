#coding=utf8
from django import forms
from orcamentos.models import Orcamento


class OrcamentoForm(forms.ModelForm):


    class Meta:
        model = Orcamento
        fields = '__all__'
        exclude = ['produtos', 'servicos', 'tipo']
        widgets = {
            'produtos': forms.Select({'class': 'form-control'}),
            'comprador': forms.Select({'class': 'form-control'}),
            'vendedor': forms.Select({'class': 'form-control'}),
            'proposta': forms.Select({'class': 'form-control'})
        }