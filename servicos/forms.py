# coding=utf-8
from .models import Servico
from django import forms


class ServicoForm(forms.ModelForm):
    class Meta:
        model = Servico
        fields = "__all__"
        widgets = {
            'descricao': forms.TextInput({'class': 'form-control', 'placeholder': 'Descrição'}),
            'valor': forms.TextInput({'class': 'form-control', 'placeholder': 'Valor'})
        }

