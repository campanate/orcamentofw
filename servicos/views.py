# coding=utf-8
from django.shortcuts import render
from django.core.paginator import Paginator
from .models import Servico
from .forms import ServicoForm
from django.shortcuts import get_object_or_404


def index(request, page=None):
    page = page or 1
    lista = Servico.objects.all()
    paginator = Paginator(lista, 10)
    return render(request, 'servicos/index.html', {'servicos': paginator.page(page), 'title': 'Lista de Serviços'})


def create(request):
    form = ServicoForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return index(request)
    return render(request, 'servicos/create.html', {'form': form, 'title': 'Novo serviço'})


def details(request, servico_id):
    servico = get_object_or_404(Servico, pk=servico_id)
    return render(request, 'servicos/details.html', {'servico': servico, 'title': 'Detalhes de serviço'})


def edit(request, servico_id):
    servico = get_object_or_404(Servico, pk=servico_id)
    form = ServicoForm(request.POST or None, instance=servico)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return index(request)
    return render(request, 'servicos/edit.html', {'form': form, 'title': 'Editar serviço'})


def delete(request, servico_id):
    servico = get_object_or_404(Servico, pk=servico_id)
    servico.delete()
    return index(request)